package seleniumFrameWork;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Waitforelement {
	WebDriver driver;
	WebDriverWait wait;
	
    public Waitforelement(WebDriver driver) {
    	 wait=new WebDriverWait(driver,60);
    	this.driver=driver;
    }
    
    
    public void waitForElement(By by) {
    	wait.until(ExpectedConditions.elementToBeClickable(by));
    }
}
