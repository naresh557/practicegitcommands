package seleniumFrameWork;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class Formouseoveranddragdrop {
//WebDriver driver;
Actions actions;
WebDriverWait waits;
int Timeout=10;
	@Test
	public void mouseover(WebDriver driver) {
		actions=new Actions(driver);
		//this.driver=driver;
		//if we have webelement eement write full element
		//after performing waits use waits
		actions.moveToElement(driver.findElement(By.className("dsfdsf"))).build().perform();
		
		//to perform drag and drop
		//first we need to click the file and holdso use click and hold then move the element to destination element and relase 
		actions.clickAndHold(driver.findElement(By.className("fdf"))).moveToElement(driver.findElement(By.className("dsfdsf"))).release().build().perform();
		//actions.
		
		//to click on back and forward arrows use navigation class for referesh also use navigate
		driver.navigate().back();
		driver.navigate().forward();
		driver.navigate().to("dfds");//is used to launch external url
		waits=new WebDriverWait(driver, Timeout);
		//driver.findElement(By.className("fsdf")).wait();
		
		               //browser handles
		
		//to handle browser alerts use pouptest.com
		Set<String> handler=driver.getWindowHandles();//set doesn't store in indexes we need to use iterator
		Iterator<String> it=handler.iterator();
		String parent=it.next();//will return the next element in iterationi.e first// it will link at top at first when we create iterator
		//it will store next element i.e child
		String Child=it.next();
		//now it is in popup so get the title and closde
		driver.switchTo().window(Child);
		driver.getTitle();
		driver.close();
		//again we need to switch to parent
		driver.switchTo().window(parent);
		
		
	}
}
