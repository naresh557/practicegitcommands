package seleniumFrameWork;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class AssignementAlerts {
	String Browser=null;
	WebDriver driver;
	WebDriverUtils utils=new WebDriverUtils();
	AlertHandles alerts=new AlertHandles(driver);
	By Jsalert = By.linkText("Click for JS Alert");
	//List<WebElement> myList = new ArrayList();
	By JsConfirm=By.linkText("Click for JS Confirm");
	By jsPrompt=By.linkText("Click for JS Prompt");
	By alertLink=By.linkText("JavaScript Alerts");

	@Test
	public void alerts() {
		driver=utils.getBrowser("ch");
		utils.loadUrl("https://www.eureqatest.com/training/");
		utils.clickonElement(alertLink);
		utils.clickonElement(Jsalert);
		alerts.handleAnyAlert();
		utils.clickonElement(JsConfirm);
		alerts.handleAnyAlert();
		utils.clickonElement(jsPrompt);
		alerts.handleAnyAlert();
		utils.quitBrowser();
		}

}
