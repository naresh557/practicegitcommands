package seleniumFrameWork;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;

public class AlertHandles {

	WebDriver driver;
	Alert alert ;
	
	public AlertHandles(WebDriver driver) {
		this.driver = driver;
	}
	
	public void acceptAlert() {
		driver.switchTo().alert().accept();
	}
	
	public void dismissAlert() {
		driver.switchTo().alert().dismiss();
	}
	
	public String getAlterText() {
		return driver.switchTo().alert().getText();
	}
	
	public void handleAnyAlert() {
		String alertText = getAlterText();
		
		if(alertText.toLowerCase().contains("alert")) {
			acceptAlert();
		}
		else if(alertText.toLowerCase().contains("confirm")) {
			dismissAlert();
		}
		else if(alertText.toLowerCase().contains("prompt")) {
			driver.switchTo().alert().sendKeys(alertText);
			acceptAlert();
		}
	}
}
