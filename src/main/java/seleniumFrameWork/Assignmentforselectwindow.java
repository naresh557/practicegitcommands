package seleniumFrameWork;

import java.util.List;

//import java.awt.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Assignmentforselectwindow {
	WebDriver driver;
	String BrowserName="ch";
	WebDriverUtils utils=new WebDriverUtils();
	By username=By.id("loginForm:userId");
	By password=By.id("loginForm:password");
	By Submit=By.id("loginForm:loginButton");
	 By more=By.id("header_projectClickMoreOptions");
	 By restdocs=By.id("commonheader:headerForm:lnk_restDocs");
	 By firstwindow=By.id("header_userActions");
	 By logout=By.xpath("//*[@id=\\\"header_userActionsSettingOptions\\\"]/a[2]");


	
	
	@Test
	public void selectWindow() {
		driver=utils.getBrowser(BrowserName);
		utils.loadUrl("https://ahm.eureqatest.com/");
		//driver.getTitle().toLowerCase().contains("eureqa"))
		Assert.assertTrue(driver.getTitle().toLowerCase().contains("eureqa"),"page not loaded");
		
		}
	@Test(dependsOnMethods="selectWindow()")
	public void loginintoapplication() {
		utils.sendKeystoElement(username, "nveldae");
		utils.sendKeystoElement(password, "123456");
		utils.clickonElement(Submit);
		Assert.assertTrue(driver.getTitle().toLowerCase().contains("eureqa"), "page not loaded properly");
	}
	@Test(dependsOnMethods="loginintoapplication()")
	public void assignment() {
		//use here mouse over command
		driver.findElement(more);
		utils.clickonElement(restdocs);
	     String winHandleBefore = driver.getWindowHandle();
	     driver.switchTo().window("Automation Execution");
	     driver.findElement(restdocs).getAttribute(BrowserName);
	     List<WebElement> links = driver.findElements(By.xpath(".//*"));
	    // int count=links.size();
	     for(WebElement e : links)
	     {
	    	  System.out.println(e.getText());
	    	  e.click();
	    	  		 
	    	  		 //list<WebElement> a=driver.findElements(Submit);
	    	  		 }
			driver.switchTo().window(winHandleBefore);
			
utils.clickonElement(firstwindow);	
utils.clickonElement(logout);
	     utils.quitBrowser();

}
}
