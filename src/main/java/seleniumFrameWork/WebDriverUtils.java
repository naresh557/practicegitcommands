package seleniumFrameWork;

import java.io.File;
import java.io.IOException;
//import java.io.File;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.support.ui.Select;

public class WebDriverUtils {
	
	WebDriver driver;
	Waitforelement Waits;
	
	public  WebDriver getBrowser(String browserName) {
		//System.setProperty("webdriver.chrome.driver","C:\\NareshProject\\chromedriver_win32\\chromedriver.exe");
		if(browserName.toLowerCase().contains("ch"))
		{
			System.setProperty("webdriver.chrome.driver","C:\\NareshProject\\chromedriver_win32\\chromedriver.exe");
			 driver=new ChromeDriver();

		}else if(browserName.toLowerCase().contains("ff"))
		{
			System.setProperty("webdriver.gecko.driver","C:\\NareshProject\\chromedriver_win32\\chromedriver.exe");
			 driver=new FirefoxDriver();

		}else if(browserName.toLowerCase().contains("ch"))
		{
			System.setProperty("webdriver.ie.driver","C:\\NareshProject\\chromedriver_win32\\chromedriver.exe");
			 driver=new InternetExplorerDriver();

		}
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		Waits=new Waitforelement(driver);//need to initialize after assinging the driver
		return driver;
	}	
	
	   public void loadUrl(String Url)
	   {
		   driver.get(Url); 	   
	   }
	   
	   public WebElement getWebElement(By by) 
	   {
		   return driver.findElement(by);
	   }
	   
		public void sendKeystoElement(By by,String inputValue) 
		{
			WebElement element=getWebElement(by);
			if(element.isEnabled()) {
				element.clear();
				element.sendKeys(inputValue);
	
			}
		}
		
		public void clickonElement(By by) 
		{
			WebElement element=getWebElement(by);
			if(element.isEnabled()) {
				//element.clear();
				element.click();

			}
			
		}
		public void dropdownSelect(By by,String Value) {
			Select a=new Select(driver.findElement(by));
			a.selectByValue(Value);
			
		}
		
		public void Screenshots(String FileName) throws Exception
		{
         TakesScreenshot ts=(TakesScreenshot)driver;
         File source= ts.getScreenshotAs(OutputType.FILE);
         //i am able to use fileutils.copyfile???????????
         FileHandler.copy(source,new File(System.getProperty("user.dir")+"/images/"+FileName+".png"));
       
		}
		
		//if you want to uplad a file which is in desktop then don't click use sendkeys give filepth
		public void upladfile(By by,String filepath) {
			driver.findElement(by).sendKeys(filepath);
		}
		
		//to switch frames
		public void frame() {
			//if we have frame webelement use total xpath for it
			driver.switchTo().frame(driver.findElement(By.className("fd")));
			//if we use frame use 2 to 3 sec pause before and after 
			driver.switchTo().frame("erer");
		}
		public void quitBrowser() {
			driver.quit();
		}
		
		//wecan use config.properties file where we can save default values i.e 
		//browser=ch or url ike that 
		//we have properties is default class only we need to createobject
		//propertied prop=new properties();
		//prop.getproperty("${variablename}")
		//here -ve is every time we need to change browser or url else we can use common method and send in test case
		      //htmlunitbrowser
		//advantages
		//no browser launch,its very fast ,improvre performance of script,testing is behind the scene,not suiyale for actions
		//it is also called headless browser i.e htmlunit driver
		//htmldriver-java
		//phantonjs-jAVascript
		//from selenium 3.0 we need to download htmlunit driver jar//in 2.x versions default we have htmlunit drivers
                 
		//1. to highlight for specific element we need to use java script executor
		public static void flash(By by, WebElement element,WebDriver driver) {//here if i give by y it not coming??????????
			
		JavascriptExecutor js=(JavascriptExecutor)driver;
		//driver.findElement(by).getCssValue("");
		String color=element.getCssValue("backgroundcolor");
		js.executeScript("arguments[0].style.backgroundcolor=rgb(0,200,0)", element);
		//by using javascript we can execute everything
		js.executeScript("arguments[0].click", element);	}
} 
//right click on above element and check whether it as frame or not .by selecting view page source we can see fram are available or not


	 



