package seleniumFrameWork;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class Mouseover {
	Actions actions;
	WebDriver driver;
	
	public Mouseover(WebDriver driver) {
		actions =new Actions(driver);
		this.driver=driver;
	}
	
	public void actionsmethod(WebElement element) {
		
		actions.moveToElement(element);
		actions.build().perform();

	}

}
