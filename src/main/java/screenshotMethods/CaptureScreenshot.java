package screenshotMethods;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;


public class CaptureScreenshot 
{
	
	public static void getScreenshotAsFile(WebDriver driver, String fileName)
	{
		TakesScreenshot takeScreenShot = (TakesScreenshot)driver;
		File captured = takeScreenShot.getScreenshotAs(OutputType.FILE);
		try {
			
		FileUtils.copyFile(captured, new File(System.getProperty("user.dir") + "\\"+fileName+ ".png"));
		}catch(Exception e) {
			System.out.println("fdsfa");
		}
	}
	

}
